package org.launchcode.training;


import org.junit.Test;

import static org.junit.Assert.*;

public class CarTest {

    @Test
    public void testConstructor() {
        //TODO: constructor sets gasTankLevel properly
        Car test_car = new Car("Toyota", "Prius", 10, 50);
        assertEquals(10, test_car.getGasTankLevel(), .001);
    }

    //TODO: gasTankLevel is accurate after driving within tank range
    @Test
    public void testGasLevelAfterDriving() {
        Car test_car = new Car("Ford", "Mustang", 15, 20);
        test_car.drive(20);
        assertEquals(14, test_car.getGasTankLevel(), .001);

    }
    //TODO: gasTankLevel is accurate after attempting to drive past tank range
    @Test
    public void testGasTankZeroAfterDriving() {
        Car test_car = new Car("a", "a", 1, 10);
        test_car.drive(11);
        assertEquals(0, test_car.getGasTankLevel(), .001);
    }
    //TODO: can't have more gas than tank size, expect an exception
    @Test(expected = IllegalArgumentException.class)
    public void testGasCantExceedTankSize() {
        Car test_car = new Car("a", "a", 5, 10);
        test_car.addGas(20);
        fail("Shouldn't get here, car cannot have more gas in tank than the size of the tank");

    }

}
